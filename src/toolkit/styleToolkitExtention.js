/* esversion: 6 */

// extends toolkit functions to modify styles.

/**
 * Extends base toolkit with stlye modification capability's
 * @extends {toolkit}
 */
export class styleToolkitExtention {

  /**
   * Sets up the members for this extention.
   * @public
   */
  initializer(){
    this.addExtention("styleToolkitExtention");
    /**
     * Enum of options for the modification of the class.
     * @public
     * @constant
     * @type {ENUM}
     * @property {number} ON
     * @property {number} OFF
     * @property {number} TOGGLE
     */
    this.tri = {
      ON: 1,
      OFF: 0,
      TOGGLE: 0.5
    }
  }

  // replaces the style values
  /**
   * Changes style value of selected elements.
   * @public
   * @param {String} property - what CSS property you wish to change.
   * @param {String} [value=""] - what you want to set the property to. If empty string, removes the element override.
   */
  changeStyle(property, value) {
    this.hasElement();
    value = value || "";
    this.doEach( function (element, index) {
      element.style[property] = value;
    });
  }

  /**
   * Adds / Removes / Toggles classes attached to elements.
   * @public
   * @param {String} className - The class to add / remove / toggle
   * @param {ENUM} [fixed=TOGGLE] - If you would like to add / remove / toggle the class.
   * @throws {String} If fixed is a invalid ENUM_TRI, throws a error.
   */
  modifyClass(className, fixed) {
    this.hasElement();
    fixed = fixed || this.tri.TOGGLE;
    let f = function(element, index){throw "This should never happen."};
    switch(fixed){
      case this.tri.ON:
        f = function(element, index){element.classList.add(className);};
        break;
      case this.tri.OFF:
        f = function(element, index){element.classList.remove(className);};
        break;
      case this.tri.TOGGLE:
        f = function(element, index){element.classList.toggle(className);};
    }
    this.doEach(f);
  }
};
