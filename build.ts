import { emptyDir, walk, copy, ensureDir } from "https://deno.land/std@0.97.0/fs/mod.ts"; 
//import { Language, minify, minifyHTML } from "https://deno.land/x/minifier/mod.ts";
import { generateComics, saveComics } from "./generators/comicIndex.ts";

const HTMLTest = /.*\.html/;
const CSSTest = /.*\.css/;


{
  const generatorPromise = generateComics();
  const path = /static\//;
  await emptyDir("./public");
  for await (const entry of walk("./static")){
    if(entry.isDirectory){
      continue;
    }
    const writePath = entry.path.replace(path, "public/");
    const ensure = ensureDir(writePath.replace(entry.name, ""));
    if(HTMLTest.test(entry.name)){
      const code = await Deno.readTextFile(entry.path);
      //Triggering some unreachable error...
      //await Deno.writeTextFile(writePath, minify(Language.HTML, code));
      // const minified = await minifyHTML(code, {
      //   minifyCSS: true,
      //   minifyJS: true,
      // });
      await ensure;
      // await Deno.writeTextFile(writePath, minified);
      await Deno.writeTextFile(writePath, code);
      continue;
    }
    if(CSSTest.test(entry.name)){
      const code = await Deno.readTextFile(entry.path);
      await ensure;
      //await Deno.writeTextFile(writePath, minify(Language.CSS, code));
      await Deno.writeTextFile(writePath, code);
      continue;
    }
    await ensure;
    await copy(entry.path, writePath, {overwrite: true});
  }

  
  const { files, diagnostics } = await Deno.emit(
    `./src/orbMap.ts`,
    {
      bundle: "module",
      check: false,
      compilerOptions: {
        sourceMap: true
      }
    }
  )

  if (diagnostics.length) {
    // there is something that impacted the emit
    console.warn(Deno.formatDiagnostics(diagnostics));
  } else {
    //Deno.writeTextFile("./public/orbMap.js", minify(Language.JS ,files["deno:///bundle.js"]));
    await ensureDir("./public/js/");
    Deno.writeTextFile("./public/js/orbMap.js", `//# !sourceMappingURL=./orbMap.js.map
console.log("Built at ${Date.now()}");
${files["deno:///bundle.js"]}`);
    Deno.writeTextFile("./public/js/orbMap.js.map", files["deno:///bundle.js.map"]);
    //Deno.writeTextFile("./public/js/orbMap.js", "console.warn('nope');");
  }
  await generatorPromise;
  await saveComics();
}

console.log("Bundling is done!")