import {orb} from "./mapClasses/OrbClass.ts";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be undefined except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.ts";


// Com Orb
export const comOrbNodes = [
    orb( 'CO', 'Orb', 640, 140, 'Com-Ball / Communications Orb', './images/Orbs/ComOrb.png', {
      status:"The Com-Orb",
      notes:{
        k:"ul",
        c:[
          "One unlock chain is probably range, one of the others is probably scan modes, Not sure which is which.",
          {
            k:"t",
            c: [
              "\"lightbee\" stuff",
              {
                k:"ul",
                c:[
                  "Is it immaterial?",
                  "Can it be destroyed?",
                  "Can it go through objects?",
                  "What happens if it gets hit by something?",
                  "Can it get trapped somewhere?"
                ]
              }
            ]
          }
        ]
      },
      whatItDoes:{
        k:"ul",
        c:[
          "\"Lightbee\"",
          "Remote Hologram",
          "Teleportation",
          "Truesight",
          "Mage-sight"
        ]
      }
    }),
    orb( 'CO.A.1', 'Available', 680, -100, undefined, undefined , {
      notes: {
        k:"ul",
        c:[
          "Possibly a passive ability.",
          "Universal Translator? Telepathy?"
        ]
    }
    }),
    orb( 'CO.B.1', 'Available', 750, 'zero'),
    orb( 'CO.B.2', 'Available'),
    orb( 'CO.B.3', 'Available'),
    orb( 'CO.B.4', 'Available', 980, -160),
    orb( 'CO.C.1', 'Purchased', 840, 20),
    orb( 'CO.C.2', 'Purchased'),
    orb( 'CO.C.3', 'Purchased'),
    orb( 'CO.C.4', 'Purchased'),
    orb( 'CO.C.5', 'Available', 1300, 140),
    orb( 'CO.D.1', 'Purchased', 840, 100),
    orb( 'CO.D.2', 'Available'),
    orb( 'CO.D.3', 'Available'),
    orb( 'CO.D.4', 'Available'),
    orb( 'CO.D.5', 'Available', 1120, 320),
    orb( 'CO.D.5.VP', 'VanishingPoint', 1340, 300),
    orb( 'CO.E.1', 'Purchased', 840, 260),
    orb( 'CO.E.1.A.1', 'Available', 900, 380),
    orb( 'CO.E.1.B.1', 'Available', 740, 300),
    orb( 'CO.E.1.B.2', 'Available'),
    orb( 'CO.E.1.B.3', 'Available'),
    orb( 'CO.E.1.B.4', 'Available', 700, 580),
    orb( 'CO.F.1', 'Purchased', 640, 340, undefined, undefined, {
      notes:"This is most likely the teleport function since it would probably not have any further unlocks",
      ull: true,
      unlocked: {
        k:"a",
        p:'183'
      }
    })
];

export const comOrbEdges = [
    edge( 'CO', 'CO.A.1', "En", {dashed: true}),
    edge( 'CO', 'CO.B.1', "En"),
    edge( 'CO.B.1', 'CO.B.2', 'En'),
    edge( 'CO.B.2', 'CO.B.3', 'En'),
    edge( 'CO.B.3', 'CO.B.4', 'En'),
    edge( 'CO', 'CO.C.1', "En"),
    edge( 'CO.C.1', 'CO.C.2', 'En'),
    edge( 'CO.C.2', 'CO.C.3', 'En'),
    edge( 'CO.C.3', 'CO.C.4', 'En'),
    edge( 'CO.C.4', 'CO.C.5', 'En'),
    edge( 'CO', 'CO.D.1', "En"),
    edge( 'CO.D.1', 'CO.D.2', 'En'),
    edge( 'CO.D.2', 'CO.D.3', 'En'),
    edge( 'CO.D.3', 'CO.D.4', 'En'),
    edge( 'CO.D.4', 'CO.D.5', 'En', {dashed: true}),
    edge( 'CO.D.5', 'CO.D.5.VP', 'Dis', {arrow: true}),
    edge( 'CO', 'CO.E.1', "En"),
    edge( 'CO.E.1', 'CO.E.1.A.1', 'En'),
    edge( 'CO.E.1', 'CO.E.1.B.1', 'En'),
    edge( 'CO.E.1.A.1', 'CO.E.1.B.1', 'En'),
    edge( 'CO.E.1.B.1', 'CO.E.1.B.2', 'En'),
    edge( 'CO.E.1.B.2', 'CO.E.1.B.3', 'En'),
    edge( 'CO.E.1.B.3', 'CO.E.1.B.4', 'En'),
    edge( 'CO', 'CO.F.1', "En")
];
