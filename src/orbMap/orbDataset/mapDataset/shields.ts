import {orb} from "./mapClasses/OrbClass.ts";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be undefined except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.ts";


// Mr Bubble Orb
export var MBNodes = [
    orb( 'MB', 'Orb', -640, 140, 'Shield / Mr Bubble Orb', './images/Orbs/MrBubble_Orb.png', {
      status:"Nothing gets through Mr Bubble!",
      whatItDoes:{
        k:"ul",
        c:[
          "Airtight Shield capible of withstanding most attacks",
          "When Shield is under heavy stress, it can go into a recharge mode, takes moments to return to full."
        ]
      }
    }),
    orb( 'MB.A.1', 'Available', -680, 400, undefined, undefined , {
      notes: "Possibly a passive ability."
    }),
    orb( 'MB.B.1', 'Purchased', -780, 320, undefined, undefined, {
      notes:"Max-Size upgrades?"
    }),
    orb( 'MB.B.2', 'Purchased'),
    orb( 'MB.B.3', 'Purchased'),
    orb( 'MB.B.4', 'Purchased'),
    orb( 'MB.B.5', 'Available', -1240, 414),
    orb( 'MB.C.1', 'Available', -860, 220),
    orb( 'MB.C.2', 'Available'),
    orb( 'MB.C.3', 'Available', -1000, 296),
    orb( 'MB.C.3.VP', 'VanishingPoint', -1100, 160),
    orb( 'MB.D.1', 'Available', -940, 80)
];

export var MBEdges = [
    edge('MB', 'MB.A.1', "En", {dashed: true}),
    edge('MB', 'MB.B.1', "En"),
    edge('MB.B.1', 'MB.B.2', "En"),
    edge('MB.B.2', 'MB.B.3', "En"),
    edge('MB.B.3', 'MB.B.4', "En"),
    edge('MB.B.4', 'MB.B.5', "En"),
    edge('MB', 'MB.C.1', "En"),
    edge('MB.C.1', 'MB.C.2', "En"),
    edge('MB.C.2', 'MB.C.3', "En", {dashed: true}),
    edge('MB.C.3', 'MB.C.3.VP', "Dis", {arrow: true}),
    edge('MB', 'MB.D.1', "En")
];
