/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>
import { DataSet } from 'https://cdn.skypack.dev/vis-data/esnext?dts';
import { Network } from 'https://cdn.skypack.dev/vis-network/esnext?dts';

// Import the dataset.
import {orbDataset} from "./orbDataset/importMe.ts";

/* end imports */

declare global {
    interface Window {
        nodes:any;
        edges:any;
        network:any;
    }
}

export function makeMap(): Network {

  // the nodes and edges (connections between nodes) come from the orb dataset.
  const nodes = window.nodes = new DataSet(orbDataset.nodes);
  const edges = window.edges = new DataSet(orbDataset.edges);

  // get the element where we are going to be putting the map into
  const container = document.getElementById('dataVisualization');
  if(container === null){
    throw new Error("Couldn't find container!");
  }

  // provide the data in the vis format
  const data = {
      nodes: nodes,
      edges: edges
  };

  // the options for the entire network
  const options = {
      // the groups for the network
      groups: orbDataset.nodeStyles,

      // this enables use of keyboard and displays the navigation buttons.
      interaction: {
        navigationButtons: true,
        keyboard: true
      },

      layout:{
        // this ensures that the map looks as it should
        improvedLayout: false,
        // if you add new nodes, change this until you don't have overlapping stuff.
        randomSeed: 0
      }
    };

  // initialize the map!
  const network = window.network = new Network(container, data, options);
  
  //pass back the network for further use.
  return network;
}
