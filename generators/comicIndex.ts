import { DOMParser, Element } from "https://raw.githubusercontent.com/b-fuze/deno-dom/master/deno-dom-wasm.ts";
import { ensureDir } from "https://deno.land/std@0.97.0/fs/mod.ts";

const comicNumber = /((Grrl Power #)(\d+)( – )(.{0,}))/;
const validHref = new RegExp(`(https?:\/\/(www.)?grrlpowercomic.com\/archives\/comic\/(.*))`);

import { comicDetails } from "./comicDetails.d.ts";

let comics: string;

export async function generateComics() {
    const autoComics: Record<string, comicDetails> = {};

    for(let i = 2010; i <= 2021; i++){
        console.log(`Processing comics in year ${i}...`)
        const document = new DOMParser().parseFromString(
            await fetch(`https://grrlpowercomic.com/archive/?archive_year=${i}`).then(resp=>resp.text()),
            "text/html"
        );
        if(!document){
            throw new Error(`Unable to get index for year ${i}.`);
        }
        const links = document.querySelectorAll("table.month-table td.archive-title a")
        for(const linkNode of links){
            const linkElem = linkNode as Element;
            const href = linkElem.getAttribute("href");
            if(!href){
                continue;
            }
            const vhr = href.match(validHref)
            if(!vhr){
                continue;
            }
            const text = linkElem.textContent;
            const cnm = text.match(comicNumber);
            if(!cnm){
                continue;
            }
            autoComics[cnm[3]] = {
                number: cnm[3],
                title: cnm[5],
                id: vhr[3],
            }
        }
    }

    comics = JSON.stringify(autoComics, null, 4)
}


export async function saveComics() {
    await ensureDir("./public/data/");
    await Deno.writeTextFile("./public/data/autoComicSet.json", comics);
}