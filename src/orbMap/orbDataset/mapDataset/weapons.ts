import {orb} from "./mapClasses/OrbClass.ts";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be undefined except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.ts";

// PPO Orb
export var PPONodes = [
    orb( 'PPO', 'Orb', 'zero', -680, 'PPO / Weapon Orb', './images/Orbs/PPO_Orb.png', {
      status:"PPO; Only if you'r hip.",
      whatItDoes:{
        k:"ul",
        c:[
          "Cutting Beam Weapon",
          "Scatter / Rapid Fire Beam Weapon"
        ]
      }
    }),
    orb( 'PPO.A.VP', 'VanishingPoint', -340, -640),
    orb( 'PPO.B.1', 'Available', -280, -740, undefined, undefined , {
      notes: "Possibly a passive ability."
    }),
    orb( 'PPO.C.1', 'Purchased', -180, -820, undefined, undefined, {
      notes:"This could be max power."
    }),
    orb( 'PPO.C.2', 'Purchased'),
    orb( 'PPO.C.3', 'Purchased'),
    orb( 'PPO.C.4', 'Available'),
    orb( 'PPO.C.5', 'Available', -480, -1100),
    orb( 'PPO.D.A', 'Purchased', 'zero', -900, undefined, undefined, {
      whatItDoes: "Probably the cutting beam.",
      notes:{
        k:"ul",
        c:[
          "This section of the tree is probably for the different modes the beam can use.",
          "Exact positions are not guaranteed due to DabeB not being consistent with sides."
        ]}
    }),
    orb( 'PPO.D.A.1', 'Purchased', undefined, undefined, undefined, undefined, {
      notes:"Could this allow changing the <em>power</em> of the beam?",
    }),
    orb( 'PPO.D.B', 'Available', -140, -940),
    orb( 'PPO.D.B.1', 'Available'),
    orb( 'PPO.D.C', 'Available', -140, -1060),
    orb( 'PPO.D.C.1', 'Available'),
    orb( 'PPO.D.D', 'Available', 'zero', -1100),
    orb( 'PPO.D.D.1', 'Available'),
    orb( 'PPO.D.E', 'Available', 140, -1060),
    orb( 'PPO.D.E.1', 'Available'),
    orb( 'PPO.D.F', 'Purchased', 140, -940, 'Rapid Fire', undefined, {
      whatItDoes:"Unlocks Rapid fire option.",
      notes:"Does this confirm that this unlocks alt-modes for the beam?",
      ull: true,
      unlocked: {c:[
          {
            k:"a",
            p:'510'
          },
          {
            k:"sup",
            c:{c:[
              "revealed",
              {
                k:"a",
                p:'659'
              }
            ]}
          },
        ]}
    }),
    orb( 'PPO.D.F.1', 'Available', undefined, undefined, undefined, undefined, {
      notes:"Could this allow changing the <em>rate</em> of fire?",
    })
];
export var PPOEdges = [
    edge('PPO', 'PPO.A.VP', "Dis", {arrow: true}),
    edge('PPO', 'PPO.B.1', "En", {dashed: true}),
    edge('PPO', 'PPO.C.1', "En"),
    edge('PPO.C.1', 'PPO.C.2', "En"),
    edge('PPO.C.2', 'PPO.C.3', "En"),
    edge('PPO.C.3', 'PPO.C.4', "En"),
    edge('PPO.C.4', 'PPO.C.5', "En"),
    edge('PPO', 'PPO.D.A', "En"),
    edge('PPO.D.A', 'PPO.D.A.1', "En"),
    edge('PPO.D.F', 'PPO.D.A', "En"),
    edge('PPO.D.B', 'PPO.D.B.1', "En"),
    edge('PPO.D.A', 'PPO.D.B', "En"),
    edge('PPO.D.C', 'PPO.D.C.1', "En"),
    edge('PPO.D.B', 'PPO.D.C', "En"),
    edge('PPO.D.D', 'PPO.D.D.1', "En"),
    edge('PPO.D.C', 'PPO.D.D', "En"),
    edge('PPO.D.E', 'PPO.D.E.1', "En"),
    edge('PPO.D.D', 'PPO.D.E', "En"),
    edge('PPO.D.F', 'PPO.D.F.1', "En"),
    edge('PPO.D.E', 'PPO.D.F', "En")
];
