/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>
import type { MapClickEvent } from './../mapClickEvent.d.ts'; 

// the text prosessor for JSON text
import {jsonTextProsessor} from './../toolkit/jsonTextMark.js';

/* <<<<< DATASET >>>>> */

// the dataset & metadata
import {orbDataset, options, Orb, OrbMetadata} from "./orbDataset/importMe.ts";

/* <<<<< DATASET >>>>> */

/* end imports */

// TODO: make internal only functions have `__` before them

const LOADING = "Loading";
const NONE = "None";

import type { comicDetails } from "../../generators/comicDetails.d.ts";

// declare `comicPointers` as a global var.
var comicPointers: "Loading" | Record<string, comicDetails> = LOADING;
var lastNodeList: "None" | MapClickEvent = NONE;


// TODO: replace this with a function in the toolkit, after I fix the toolkit... or not.
// this will run when the script is loaded.
fetch("./data/autoComicSet.json")
.then(req => req.json())
.then(data => {
  comicPointers = data;
  console.log("pointers downloaded");
  if (lastNodeList !== NONE){
    mapClick(lastNodeList);
  }
})


/* `modLinks` function */
// modifies links so we only need to enter in a page number to get the comic number.
function modLinks(comicNumber: string) {

  // prep a empty text var for future use
  let text = "";

  // check to see if we have the pointers yet.
  if(comicPointers == LOADING){
    // this will click the submit button.
    const reloadText = 'ev = new Event("submit"); document.getElementById("searchForm").dispatchEvent(ev);';
    // if we don't, give a error.
    text = "<a onclick='" + reloadText + "' title='Click to load.'>Click to fix</a>";
  } else {
    // get the pointer object
    const pointer = comicPointers[comicNumber]

    // if it has, run normally.
    if(pointer){
      // if we found the pointer make the tag
      const url = "https://grrlpowercomic.com/archives/comic/" + pointer.id;
      const page = "#" + pointer.number;
      const hover = page + " - " + pointer.title;
      text = "<a href='" + url + "' title='" + hover + "'>" + page + "</a>";
    } else {
      // if we did not, return a error
      text = "<a title='POINTER NOT FOUND'>#" + comicNumber + "</a>";
    }
  }

  // return the tag.
  return text;
}
// end `modLinks` function


// initialise the text prosessor *after* the `modLinks` function so we can use it
// this is used for all text prosessing.
var jtp = jsonTextProsessor({linkStart: "http://grrlpowercomic.com/archives/comic/", linkModderFunction: modLinks});


/* end prep work, begin routine work */





// Find the content of a node.
// returns undefined if no node is found.
export function findNode(searchID: string): Orb | null {
  // this returns the object not the index. (thank god.)
  return orbDataset.nodes.find(function(node) {
      // if it's the node, return true, else false.
      return (node.id == searchID);
  }) || null;
}


/* start `flagDecorations` function */
// this creates the dots for the flag.
function flagDecorations(data: OrbMetadata){
  // blank dots text
  var flags = " ";

  // if a node is unlocked
  if (data.nodeUnlockedBasic) {
    // if it has a link
    if (data.nodeUnlockedLink) {
      // will this ever run anymore?
      // show the `unlockWithLink` flag symbol
      flags += "<span title='Unlock Time W Link'>" + options.settings.symbols.flag.unlockWithLink + "</span> ";
    } else {
      // otherwise show the `unlockWithoutLink` symbol
      flags += "<span title='Unlock Time W/O Link'>" + options.settings.symbols.flag.unlockWithoutLink + "</span> ";
    }
  }

  // if we know what it does
  if (data.nodeWhatItDoes) {
    // show the `whatItDoes` symbol
    flags += "<span title='Known Function'>" + options.settings.symbols.flag.whatItDoes + "</span> ";
  }

  // and if we have notes
  if (data.nodeNote) {
    // show that it has notes.
    flags += "<span title='Has Theories'>" + options.settings.symbols.flag.hasNotes + "</span> ";
  }
  // remove the trailing whitespace
  flags = flags.slice(0, -1);

  // return the decorations
  return flags;
}
/* end `flagDecorations` function */



/* start `runTextProsessor` function */
// it runs the text prosessor when the variable is not null... what more are you expecting
  //deno-lint-ignore no-explicit-any
function runTextProsessor(data: any) {
  if (data !== undefined) {
    // if there is data run the prosessor.
    return jtp.prosessText(data);
  } else {
    return null;
  }
}
/* end `runTextProsessor` function */


function getAndReplace(id: string, text: string){
  const elem = document.getElementById(id)
  if(!elem){
    console.warn(`Couldn't find '#${id}'`)
    return;
  }
  elem.innerHTML= text;
}


/* start `setWindowText` function */
// sets the orb data pane's text.
function setWindowText(
  name: string,
  color: string | undefined,
  flag: string,
  status: string | undefined,
  unlock: string | undefined,
  whatItDoes: string | undefined,
  note: string | undefined,
  param: string | undefined ) {

  // set the name
  getAndReplace("NID", name);

  // set the flag color
  {
    const elem = document.getElementById("NFlag")
    if(!elem){
      console.warn(`Couldn't find '#NFlag'`)
      return;
    }
    elem.style.backgroundColor = color || "";
  }

  // set the flag decorations
  getAndReplace("NFlag", flag);

  // set the status
  if(status)
  getAndReplace("NStatus", status);

  // set the unlock date
  if(unlock)
  getAndReplace("NObtain", unlock);

  // set what it does
  if(whatItDoes)
  getAndReplace("NFunction", whatItDoes);

  // set the note
  if(note)
  getAndReplace("NHypothesis", note);

  //if param, add it to the url bar (or wipe them all)
  //TODO: Fix this.
  if(param){
    //new tk().setParams('node=' + param);
  } else {
    //new tk().setParams('');
  }
}
/* end `setWindowText` function */



/* start `nameGenerator` function */
function nameGenerator(clickedNode: Orb, metadata: OrbMetadata) {
  let tempText = <string>metadata.namePatern;
  let tempID = clickedNode.id;
  if(typeof metadata.beautifulID == "string"){
    tempID = <string>clickedNode.data.beautifulID;
  }
  tempText = tempText.replace("|id|", tempID);
  tempText = tempText.replace("|label|", clickedNode.label || "");
  return tempText.replace("|name|", metadata.name || "");
}
/* end `nameGenerator` function */

/* start `mapClick` function */
/* this runs when the map is clicked */
export function mapClick(properties: MapClickEvent) {

  console.log(properties.edges);

    // Save the properties so we can re-run the function if we need to.
    lastNodeList = properties;

    // the id for the node
    var ids = properties.nodes;
    if (ids.length >= 1 ){
      let clickedNode: Orb | null = null;
      let finalMD: OrbMetadata;
      //if there is a ID
        // if there is only one node
        if (ids.length == 1){
          // grab the node ID
          const searchID = ids[0];
          // then find the node and save it as `clickedNode`
          clickedNode = findNode(searchID);
        }

        if(clickedNode == null){
          noNodeMD();
          return;
        }

        /* combine metadata until we get only one object */
        // default metadata is in OrbMetadata.js
        {
            // declare the combined objects *before* sending data to them.
            let groupDefaults: OrbMetadata = options.defaultMD;
            // the node's group default metadata
            if (options.groupMD[clickedNode.group] !== undefined){
              groupDefaults = options.groupMD[clickedNode.group];
            }

            // create the full default object as the fallthrough ({} =< default_MetaData < groupDefaults)
            // the first param is overwritten, so it needs to be a empty object.
            const defaultMD = Object.assign({}, options.defaultMD, groupDefaults);


            let nodeMD: OrbMetadata = {};
            //ensure that nsMD is a valid object. (it *can* be left unset)
            if (clickedNode.data !== undefined) {
              // otherwise we get the data.
              nodeMD = clickedNode.data;
            }

            // now we make the final metadata object that is used almost excluseivly.
            finalMD = Object.assign({}, defaultMD, nodeMD);
        }
        /* done combineing metadata */

        // run the dot's maker.
        const flagDots = flagDecorations(finalMD);

        // Name generator
        const finalName = nameGenerator(clickedNode, finalMD);

        // prosess the text that should be prosessed.
        finalMD.unlocked = <string>runTextProsessor(finalMD.unlocked);
        finalMD.whatItDoes = <string>runTextProsessor(finalMD.whatItDoes);
        finalMD.notes = <string>runTextProsessor(finalMD.notes);

        // set the window text.
        setWindowText(
          finalName,
          finalMD.color?.background,
          flagDots,
          finalMD.status,
          finalMD.unlocked,
          finalMD.whatItDoes,
          finalMD.notes,
          clickedNode.id
          );
        return;
    }
    noNodeMD();
}
/* end `mapClick` function */

function noNodeMD(): void {
    // if there is not any id, set the text as the no-node data.
    const nn = options.settings.noNode
    setWindowText(nn.name, nn.color, nn.flagDots, nn.status, nn.unlocked, nn.whatItDoes, nn.note, nn.param);
}