/* esversion: 6 */

/* "I'll bould my own jQuery! Without bloat, and ES6 Compatable!" */
/* Base toolkit class, contains all the selector code. */

/**
 * This combines classes into one.
 * @example class tk extends aggregator(toolkit, contentToolkitExtention, styleToolkitExtention, urlToolkitExtention) {};
 * @param {Class} baseClass - The base class.
 * @param {...Class} mixins - Classes to mix in.
 * @return {Class} mixed class
 */
export var aggregator = (baseClass, ...mixins) => {
  let base = class _Combined extends baseClass {
    constructor (...args) {
      super(...args)
      mixins.forEach((mixin) => {
        mixin.prototype.initializer.call(this)
      })
    }
  }
  let copyProps = (target, source) => {
    Object.getOwnPropertyNames(source)
      .concat(Object.getOwnPropertySymbols(source))
      .forEach((prop) => {
        if (prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
          return
            Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop))
      })
  }
  mixins.forEach((mixin) => {
    copyProps(base.prototype, mixin.prototype)
    copyProps(base, mixin)
  })
  return base
}

/**
 * This is the base toolkit, all extentions are built off of this one.
 * @example let tk = new toolkit("body");
 */
export class toolkit{

    /**
     * Preforms the initial search.
     * @param {DOMString} [cssSearch] - The CSS you are searching for.
     * @param {Element} [baseElement=document] - the base element to start from
     * @param {boolean} [errorOnEmpty=false] - if a error should be thrown if no elements were selected when one needed to be.
     */
    constructor(cssSearch, baseElement, errorOnEmpty) {
      /**
       * Extentions in the current toolkit (and the order they were loaded).
       * @private
       * @type {String[]}
       */
      this.extentions = ["baseToolkit"];
      /**
       * If there are elements currently selected.
       * @private
       * @type {boolean}
       */
      this.ele = false;
      /**
       * The elements currently selected.
       * @private
       * @type {Array<Element>}
       */
      this.elements = [];
      /**
       * Error constants
       * @public
       * @type {ENUM}
       */
      this.errors = {
        NO_ELEMENTS: "No Elements Selected"
      }
      errorOnEmpty = errorOnEmpty || false;
      /**
       * If a error should be thrown when there are no elements to itterate through and elements are required for the operation.
       * @public
       * @type {boolean}
       */
      this.errorOnEmpty = errorOnEmpty;
      if (cssSearch !== null && cssSearch != ""){
        baseElement = baseElement || document;
        this.elements = baseElement.querySelectorAll(cssSearch);
        if(this.elements.length > 0){
          this.ele = true;
        } else {
          this.ele = false;
        }
      }
    }

    /**
     * Adds extention to the extentions member.
     * @protected
     * @param {String} newExtention - the new extention to add to the array.
     */
    addExtention(newExtention){
      this.extentions.push(newExtention);
    }

    /**
     * Get's all current extentions.
     * @return {String[]} all current extentions.
     */
    getExtentions(){
      return this.extentions;
    }

    /**
     * Checks to see if there are elements in the system.
     *
     * If there is not and it's set to throw errors, then it throws a error.
     * If there is not but it's not set to throw errors, it fails silently.
     * @throws {this.errors.NO_ELEMENTS} no elements when elements are required.
     * @public
     */
    hasElement(){
      if(!(this.ele) && this.errorOnEmpty) {
        throw this.errors.NO_ELEMENTS;
      }
    }

    /**
     * Does a function on each element.
     * @public
     * @param {function} f - function to run on each element
     */
    doEach(f) {
      this.hasElement();
      this.elements.forEach(f);
    }

    /**
     * Switches all elements to the parrents of the old list.
     * @public
     */
    parrents() {
      this.hasElement();
      let newElements = [];
      this.doEach(function(oldElement, index){
        newElements.push(oldElement.parentElement);
      });
      this.elements = newElements;
      if (this.elements.length > 0){
        this.ele = true;
      } else {
        this.ele = false;
      }
    }

    /**
     * Searches current element list for new elements useing a CSS selector or Removes all CSS selectors.
     * @public
     * @param {DOMString} [cssSearch] - css to search for
     */
    searchChildren(cssSearch) {
      this.hasElement();
      if (cssSearch !== null){
        let newElements = [];
        this.doEach(function(oldElement, index){
          newElements = newElements.concat(Array.prototype.slice.call(oldElement.querySelectorAll(cssSearch)));
        });
        this.elements = newElements;
        if(this.elements.length > 0){
          this.ele = true;
        } else {
          this.ele = false;
        }
      } else {
        this.ele = false;
        this.elements = [];
      }
    }

}
